﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Software_Hub
{
    class DataManager
    {
        public static List<Shortcut> load()
        {
            IFormatter formatter = new BinaryFormatter();
            string route = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (File.Exists(route + @"\software-hub\software-hub.csv"))
            {
                
                Stream stream = new FileStream(route + @"\software-hub\software-hub.csv", FileMode.Open, FileAccess.Read);
                List<Shortcut> objnew = (List<Shortcut>)formatter.Deserialize(stream);

                return objnew;
            }
            else
            {
                return new List<Shortcut>();
            }
        }

        public static void seriazlize(List<Shortcut> categories)
        {
            IFormatter formatter = new BinaryFormatter();
            string route = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if(!Directory.Exists(route + @"\software-hub"))
            {
                Directory.CreateDirectory(route + @"\software-hub");
            }

            Stream stream = new FileStream(route + @"\software-hub\software-hub.csv", FileMode.Create, FileAccess.Write);

            formatter.Serialize(stream, categories);
            stream.Close();
        }

    }
}
