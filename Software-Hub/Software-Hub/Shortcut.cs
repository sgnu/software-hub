﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software_Hub
{
    [Serializable]
    public class Shortcut
    {
        public string FilePath { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
    }
}
