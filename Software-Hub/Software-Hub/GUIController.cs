﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Software_Hub
{
    public class GUIController
    {
        public List<Shortcut> shortcuts { get; set; }

        public GUIController() 
        {
            this.shortcuts = DataManager.load();

            /*Shortcut test = new Shortcut();
            test.Name = "Test";
            this.shortcuts.Add(test);*/

            MainWindow main = new MainWindow(shortcuts, this);
            main.ShowDialog();
        }

        public void DeleteShortcut(Shortcut shortcut)
        {
            shortcuts.Remove(shortcut);
        }

        public void Open(Shortcut shortcut)
        {
            if (File.Exists(shortcut.FilePath))
            {
                Process.Start(shortcut.FilePath);
            }
            else
            {
                MessageBox.Show("Der angegebene Pfad ist nicht gültig", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public string GetImagePath(Shortcut shortcut)
        {
            return shortcut.ImagePath;
        }

        public string GetFilePath(Shortcut shortcut)
        {
            return shortcut.FilePath;
        }

        public void AddToHub(Shortcut shortcut)
        {
            shortcuts.Add(shortcut);
        }

        public void saveHub(List<Shortcut> saveShortcuts)
        {
            DataManager.seriazlize(saveShortcuts);
        }

    }
}
