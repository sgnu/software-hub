﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Software_Hub
{
    /// <summary>
    /// Interaktionslogik für AddSoftwareWindow.xaml
    /// </summary>
    public partial class AddSoftwareWindow : Window
    {
        MainWindow main;
        public AddSoftwareWindow(MainWindow main)
        {
            this.main = main;
            InitializeComponent();
        }

        private void btnSearchSoftwarepath_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(@"c:");
        }

        private void btnSearchImagepath_Click(object sender, RoutedEventArgs e)
        {
            string route = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            Process.Start(route);
        }

        private void btnAddSoftware_Click(object sender, RoutedEventArgs e)
        {
            Shortcut shorcut = new Shortcut();

            shorcut.FilePath = softwarePath.Text;
            shorcut.Name = shortcutName.Text;

            string workingDirectory = Environment.CurrentDirectory;

            string originImagePath = imagePath.Text;
            string pathInProject = System.IO.Path.Combine(Directory.GetParent(workingDirectory).Parent.FullName, @"img\");

            string destFile = System.IO.Path.Combine(pathInProject, shorcut.Name + ".jpg");

            if (File.Exists(originImagePath))
            {
                System.IO.File.Copy(originImagePath, destFile, true);

                Button button = new Button();

                Thickness thickness = new Thickness(5);

                button.Margin = thickness;
                button.Width = 85;
                button.Height = 85;
                button.Click += (sender2, e2) => this.main.openSoftware(sender2, e2, shorcut);

                StackPanel stpnl = new StackPanel();
                Image img = new Image();
                Label lbl = new Label();

                img.Source = new BitmapImage(new Uri(destFile));
                img.Height = 55;

                //lbl.HorizontalContentAlignment.ToString("Center");
                lbl.HorizontalContentAlignment = HorizontalAlignment.Center;
                lbl.Content = shorcut.Name;

                stpnl.Children.Add(img);
                stpnl.Children.Add(lbl);

                button.Content = stpnl;

                this.main.addElement(sender, e, button, shorcut);

                this.Close();
            }
            else
            {
                MessageBox.Show("Das Bild welches Sie ausgewählt haben, existiert nicht", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
        }
    }
}
