﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Software_Hub
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Shortcut> shortcuts { get; set; }
        public GUIController controller;
        public MainWindow(List<Shortcut> shortcuts, GUIController controller)
        {
            this.controller = controller;
            this.shortcuts = shortcuts;
            InitializeComponent();
            this.startHub(this.shortcuts);
        }

        private void startHub(List<Shortcut> shortcuts)
        {
            string route = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (File.Exists(route + @"\software-hub\software-hub.csv"))
            {
                foreach (Shortcut shortcut in shortcuts)
                {
                    Button button = new Button();

                    string workingDirectory = Environment.CurrentDirectory;
                    string pathInProject = System.IO.Path.Combine(Directory.GetParent(workingDirectory).Parent.FullName, @"img\");
                    string destFile = System.IO.Path.Combine(pathInProject, shortcut.Name + ".jpg");

                    Thickness thickness = new Thickness(5);                    

                    button.Margin = thickness;
                    button.Width = 85;
                    button.Height = 85;
                    button.Click += (sender2, e2) => this.openSoftware(sender2, e2, shortcut);

                    StackPanel stpnl = new StackPanel();
                    Image img = new Image();
                    Label lbl = new Label();

                    img.Source = new BitmapImage(new Uri(destFile));
                    img.Height = 55;

                    //lbl.HorizontalContentAlignment.ToString("Center");
                    lbl.HorizontalContentAlignment = HorizontalAlignment.Center;
                    lbl.Content = shortcut.Name;

                    stpnl.Children.Add(img);
                    stpnl.Children.Add(lbl);

                    button.Content = stpnl;

                    WrapPanelCategories.Children.Add(button);
                }
            }
        }

        private void addNewSoftware_Click(object sender, RoutedEventArgs e)
        {
            AddSoftwareWindow new_software = new AddSoftwareWindow(this);
            new_software.ShowDialog();
        }

        public void openSoftware(object sender, RoutedEventArgs e, Shortcut shortcut)
        {
            this.controller.Open(shortcut);
        }

        public void addElement(object sender, RoutedEventArgs e, Button button, Shortcut shortcut)
        {
            WrapPanelCategories.Children.Add(button);
            this.shortcuts.Add(shortcut);
        }

        private void saveHub_Click(object sender, RoutedEventArgs e)
        {
            this.controller.saveHub(this.shortcuts);
        }
    }
}
